require 'spec_helper'

module Stubsvc
  RSpec.describe Stubsvc do
    it 'has a version number' do
      expect(Stubsvc::VERSION).not_to be nil
    end

    it 'knows its root path' do
      expect(File.exist?(Stubsvc.root)).to eq(true)
    end

    context '#stub' do
      let(:valid_params) do
        {
          article: {
            name: 'Test Article'
          }
        }
      end

      before do
        ENV['FS_BASE_URI'] = 'http://localhost:5000/api/v1'
      end

      it 'fails if environment variable not found for given service symbol' do
        ENV['FS_BASE_URI'] = nil
        expect do
          Stubsvc.stub_action(:fs, :post, :folder, valid_params.merge(id: 1), 201)
        end.to raise_error(KeyError)
      end

      it 'calls stubber.stub_action' do
        stubber = double(:stubber)
        expect(stubber).to receive(:stub_action)
        Stubsvc.stub_action(:fs, :post, :folder, valid_params.merge(id: 1), 201, stubber)
      end

      it 'calls stubber.stub_verb' do
        stubber = double(:stubber)
        expect(stubber).to receive(:stub_verb)
        Stubsvc.stub_verb(:fs, :post, :folder, valid_params.merge(id: 1), 201, stubber)
      end
    end
  end
end
