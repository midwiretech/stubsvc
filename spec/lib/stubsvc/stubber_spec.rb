require 'spec_helper'
require 'rest-client'

# rubocop:disable Metrics/ModuleLength
module Stubsvc
  RSpec.describe Stubber, type: :lib do
    let(:base_uri) { 'http://localhost:5000/api/v3' }
    let(:service) { :my_cool_service }
    let(:stubber) { Stubber.new(service) }
    let(:valid_params) do
      {
        article: {
          name: 'Test Article',
          id: 23
        }
      }
    end

    before do
      ENV['MY_COOL_SERVICE_BASE_URI'] = base_uri
    end

    context '#new' do
      it 'instantiates a Stubber class' do
        expect(stubber).to be_a(Stubber)
      end

      it 'sets the @service variable' do
        expect(stubber.service).to eq(service)
      end
    end

    context '.base_uri' do
      it 'fails if environment variable not found for given service symbol' do
        ENV['MY_COOL_SERVICE_BASE_URI'] = nil
        expect do
          stubber.base_uri
        end.to raise_error(KeyError)
      end

      it 'removes the ending slashes' do
        ENV['MY_COOL_SERVICE_BASE_URI'] = "#{base_uri}//"
        expect(stubber.base_uri).to eq(base_uri)
      end

      it 'returns the base URI' do
        expect(stubber.base_uri).to eq(base_uri)
      end
    end

    context '.stub_action' do
      context 'with unkown action' do
        it 'fails with RuntimeError' do
          expect do
            stubber.stub_action(:bogus, :folder, valid_params, 200)
          end.to raise_error(RuntimeError)
        end
      end

      context 'with ambiguous action' do
        it 'fails with RuntimeError' do
          expect do
            stubber.stub_action(:get, :folder, valid_params, 200)
          end.to raise_error(RuntimeError)
        end
      end

      context 'without :id in the path' do
        context 'for :create' do
          let(:response_code) { 201 }

          before do
            stubber.stub_action(:create, :folder, valid_params, response_code)
          end

          context 'stubs the request and' do
            it 'returns the expected http code' do
              response = RestClient.post("#{base_uri}/folders", valid_params)
              expect(response.code).to eq(response_code)
            end

            it 'returns the expected JSON' do
              response = RestClient.post("#{base_uri}/folders", valid_params)
              expected_hash = Oj.load(response)
              expect(expected_hash).to eq(valid_params)
            end
          end
        end

        context 'for :index' do
          let(:response_code) { 200 }

          before do
            stubber.stub_action(:index, :folder, valid_params, response_code)
          end

          context 'stubs the request and' do
            it 'returns the expected http code' do
              response = RestClient.get("#{base_uri}/folders", valid_params)
              expect(response.code).to eq(response_code)
            end

            it 'returns the expected JSON' do
              response = RestClient.get("#{base_uri}/folders", valid_params)
              expected_hash = Oj.load(response)
              expect(expected_hash).to eq(valid_params)
            end
          end
        end
      end

      context 'with :id in the path' do
        let(:id) { 999 }
        let(:response_code) { 200 }

        context 'for :show' do
          before do
            valid_params[:article][:id] = id
            stubber.stub_action(:show, :folder, valid_params, response_code)
          end

          context 'stubs the request and' do
            it 'returns the expected http code' do
              response = RestClient.get("#{base_uri}/folders/#{id}", valid_params)
              expect(response.code).to eq(response_code)
            end

            it 'returns the expected JSON' do
              response = RestClient.get("#{base_uri}/folders/#{id}", valid_params)
              expected_hash = Oj.load(response)
              expect(expected_hash).to eq(valid_params)
            end
          end
        end

        context 'for :destroy' do
          before do
            valid_params[:article][:id] = id
            stubber.stub_action(:destroy, :folder, valid_params, response_code)
          end

          context 'stubs the request and' do
            it 'returns the expected http code' do
              response = RestClient.delete("#{base_uri}/folders/#{id}", valid_params)
              expect(response.code).to eq(response_code)
            end

            it 'returns the expected JSON' do
              response = RestClient.delete("#{base_uri}/folders/#{id}", valid_params)
              expected_hash = Oj.load(response)
              expect(expected_hash).to eq(valid_params)
            end
          end
        end

        context 'for :update' do
          before do
            valid_params[:article][:id] = id
            stubber.stub_action(:update, :folder, valid_params, response_code)
          end

          context 'stubs the request and' do
            it 'returns the expected http code' do
              response = RestClient.put("#{base_uri}/folders/#{id}", valid_params)
              expect(response.code).to eq(response_code)
            end

            it 'returns the expected JSON' do
              response = RestClient.put("#{base_uri}/folders/#{id}", valid_params)
              expected_hash = Oj.load(response)
              expect(expected_hash).to eq(valid_params)
            end
          end
        end
      end
    end

    context '.stub_verb' do
      it 'stubs an arbitrary URI' do
        stubber.stub_verb(:get, '/some/bogus/uri', valid_params, 200)
        response = RestClient.get("#{base_uri}/some/bogus/uri", valid_params)
        expect(response.code).to eq(200)
        expected_hash = Oj.load(response)
        expect(expected_hash).to eq(valid_params)
      end
    end
  end
end
