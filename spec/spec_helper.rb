$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)

if ENV['COVERAGE']
  require 'simplecov'
  SimpleCov.start do
    add_filter 'spec/'
    add_filter 'vendor/'
  end
end

require 'pry-nav'
require 'stubsvc'

RSpec.configure do |config|
  include Stubsvc

  config.mock_with :rspec
  config.color = true
  config.order = 'random'

  # Run focused examples
  config.filter_run :focus
  config.run_all_when_everything_filtered = true
end
