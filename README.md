# Stubsvc

[![Gem Version](https://badge.fury.io/rb/stubsvc.svg)](https://badge.fury.io/rb/stubsvc)

**Version: 0.1.6**

Stub calls to ActiveResource based web services using [Webmock](https://github.com/bblimke/webmock).

Testing [SOA](http://www.service-architecture.com/articles/web-services/service-oriented_architecture_soa_definition.html) - [ActiveResource](https://github.com/rails/activeresource) services can be problematic using [VCR](https://github.com/vcr/vcr). `Stubsvc` makes it to stub these kinds of RESTful calls to external services, in a deterministic way.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'stubsvc'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install stubsvc

## Usage

We've only tested using RSpec. However it may work just fine with Minitest, Test::Unit or others. Please let us know your results and pull requests are always welcome.

### Configuration

Configure your external services using environment variables as follows:

    SVC_BASE_URI='http://localhost:5000/api/v4'

Where `SVC` in the variable name above is the symbol you will use for your service, and the value points to the base URI for the particular service you are testing.

For example, let's say I have a file service that handles uploads and downloads and all things file-related. I could define the environment variable as follows:

    FS_BASE_URI='http://localhost:5000/api/v4'

We recommend using Brandon Keeper's [dotenv](https://github.com/bkeepers/dotenv) gem for defining your base URIs.

### Stubbing

There are two ways to stub a service call:

1. `Stubsvc.stub_action`
2. `Stubsvc.stub_verb`

#### stub_action

`Stubsvc.stub_action` is for Rails [ActiveResource](https://github.com/rails/activeresource) type actions and accepts the following arguments:

    Stubsvc.stub_action(service, action, resource, return_hash, status)

* service - Lowercase symbol matching the environment variable defined above minus `_BASE_URI`
* action - One of `:index, :show, :create, :update, :destroy, :where, :find`
* resource - Symbol representing the remote resource. It must be snake-cased. For example, if the resource is `RootFolder`, the symbol would be `:root_folder`
* return_hash - (optional) A hash representation of the JSON data you want returned for this call. If you don't care what gets returned and leave this argument out, it will default to `"{}"`
* status - (optional) The HTTP status code that should be returned. Defaults to 200

#### stub_verb

`Stubsvc.stub_verb` is for stubbing standard, RESTful HTTP verbs and accepts the following arguments:

    Stubsvc.stub_verb(service, http_verb, uri, return_hash, status)

* service - Lowercase symbol matching the environment variable defined above minus `_BASE_URI`
* http_verb - One of `:get, :post, :put, :delete`
* uri - A full or partial URI. It can be a regular expression or a string.
* return_hash - (optional) A hash representation of the JSON data you want returned for this call. If you don't care what gets returned and leave this argument out, it will default to `"{}"`
* status - (optional) The HTTP status code that should be returned. Defaults to 200

### RSpec Example

Let's say I have a model called `Article`. Whenever an `Article` instance is created it automatically creates a `Folder` instance by calling my external file service.

We want to unit test the `create` method for `ArticlesController`, without having to fire up the external service. We can stub out the call to the file service `Folder.create` method like this:

    RSpec.describe ArticlesController, type: :controller do
      let(:valid_params) do
        {
          article: {
            name: 'Test Article'
          }
        }
      end

      it 'persists an Article instance' do
        Stubsvc.stub(:fs, :post, :folder, valid_params.merge(id: 1), 201)
        expect do
          post :create, valid_params
        end.to change(Article, :count).by(1)
      end
    end

This effectively stubs the `POST` call to `http://localhost:5000/api/v4/folders` to simply return this JSON:

    {"article":{"name":"Test Article","id":1}}

...with a status of `201`.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
