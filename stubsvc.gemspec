# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'stubsvc/version'

Gem::Specification.new do |spec|
  spec.name          = 'stubsvc'
  spec.version       = Stubsvc::VERSION
  spec.authors       = ['Chris Blackburn']
  spec.email         = ['205abc7e@opayq.com']

  spec.summary       = 'Deterministically stub out calls to ActiveResource web services.'
  spec.description   = spec.summary
  spec.homepage      = 'https://bitbucket.org/midwiretech/stubsvc/overview'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.9'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'pry-nav'
  spec.add_development_dependency 'rspec', '~> 3.2'
  spec.add_development_dependency 'rest-client', '~> 1.8'
  spec.add_development_dependency 'simplecov', '~> 0.10'

  spec.add_dependency 'midwire_common', '~> 0.1.16'
  spec.add_dependency 'webmock', '~> 1'
  spec.add_dependency 'activesupport', '~> 4.2'
  spec.add_dependency 'oj', '~> 2.12'
  spec.add_dependency 'rack', '~> 1.6'
end
