require 'stubsvc/version'

# Syntactical sugar for Stubber instatiation.
module Stubsvc
  class << self
    def root
      Pathname.new(File.dirname(__FILE__)).parent
    end

    # rubocop:disable Metrics/ParameterLists
    def stub_action(service, action, resource, return_hash, status, stubber = Stubber.new(service))
      stubber.stub_action(action, resource, return_hash, status)
    end

    def stub_verb(service, verb, uri, return_hash, status, stubber = Stubber.new(service))
      stubber.stub_verb(verb, uri, return_hash, status)
    end
    # rubocop:enable Metrics/ParameterLists
  end

  autoload :Stubber, 'stubsvc/stubber'
end
