require 'webmock'
require 'oj'
require 'active_support/core_ext/string/inflections'

module Stubsvc
  # Wraps the `stub_request` calls to Webmock.
  class Stubber
    include WebMock::API

    attr_reader :service

    def initialize(service)
      @service = service
    end

    def stub_action(action, resource, return_hash = {}, status = 200)
      http_verb = verb_from_action(action)
      regex = regex_for_action(resource, action, return_hash)
      stub_request(http_verb, regex).to_return(
        status: status.to_i,
        body: Oj.dump(return_hash)
      )
    end

    def stub_verb(verb, uri, return_hash = {}, status = 200)
      http_verb = verb_from_action(verb)
      pattern = "#{base_uri}#{uri}"
      stub_request(http_verb, pattern).to_return(
        status: status.to_i,
        body: Oj.dump(return_hash)
      )
    end

    def base_uri
      return @base_uri if @base_uri
      env_key = "#{service.upcase}_BASE_URI"
      @base_uri = ENV.fetch(env_key).sub(%r{/+$}, '')
    end

    private

    def verb_from_action(action)
      case action.to_sym
      when :show, :index, :where, :find, :get then :get
      when :create, :post then :post
      when :update, :put then :put
      when :destroy, :delete then :delete
      else
        fail("Unknown action or http verb specified: '#{action}'")
      end
    end

    def regex_for_action(item_type, action, return_hash = {})
      pattern = "#{base_uri}/#{item_type.to_s.pluralize.downcase}"
      pattern << if requires_id_in_path?(action)
                   "/#{return_hash.fetch(:id, 1)}*"
                 else
                   '*'
                 end
      Regexp.new(pattern)
    end

    def requires_id_in_path?(action)
      case action.to_sym
      when :index, :create, :where, :post then false
      when :show, :destroy, :update, :find, :put, :delete then true
      else
        fail("Can't figure out if the path contains an id: '#{action}'")
      end
    end
  end
end
